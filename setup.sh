#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

export PATH="${PATH}:${HOME}/.local/bin"
poetry install
poetry run ansible-galaxy install -r requirements.yml -p roles
mitogen_path=$(poetry run python -c \
  'import os, ansible_mitogen;print(os.path.dirname(ansible_mitogen.__file__))' \
  | sed 's/[\/&]/\\&/g')
sed "s/<<mitogen_path>>/${mitogen_path}/" > ansible.cfg < ansible_template.cfg
